<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/images/admin/apple-icon.png') }}">
        <link rel="icon" type="image/png" href="{{ asset('/images/admin/favicon.png') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Административная панель</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="{{ asset('/css/admin/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('/css/admin/paper-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
    </head>
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="white" data-active-color="danger">
                <!-- Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"   -->
                <div class="logo">
                    <a href="http://jnsound.test" class="simple-text logo-mini">
                        <div class="logo-image-small">
                            <img src="{{ asset('/images/logo.png') }}">
                        </div>
                    </a>
                    <a href="/admin" class="simple-text logo-normal">
                    	JNSound.kz
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class={{ Request::is('admin') ? "active" : "" }}>
                            <a href="/admin/">
                                <i class="nc-icon nc-single-copy-04"></i>
                                <p>Заявки</p>
                            </a>
                        </li>
                        <li class={{ Request::is('admin/categories') ? "active" : "" }}>
                            <a href="/admin/categories/">
                                <i class="nc-icon nc-bullet-list-67"></i>
                                <p>Категории</p>
                            </a>
                        </li>
                        <li class={{ Request::is('admin/items') ? "active" : "" }}>
                            <a href="/admin/items">
                                <i class="nc-icon nc-diamond"></i>
                                <p>Товары</p>
                            </a>
                        </li>
                        <li class={{ Request::is('admin/brands') ? "active" : "" }}>
                            <a href="/admin/brands">
                                <i class="nc-icon nc-album-2"></i>
                                <p>Бренды</p>
                            </a>
                        </li>
<!--                         <li class={{ Request::is('admin/news') ? "active" : "" }}>
                            <a href="/admin/news">
                                <i class="nc-icon nc-paper"></i>
                                <p>Новости</p>
                            </a>
                        </li> -->
                        <li>
                            <a href="{{ route('logout') }}">
                                <i class="nc-icon nc-button-power"></i>
                                <p>Выйти</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            @yield('breadcrumbs')
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="{{ asset('/js/admin/core/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/admin/core/popper.min.js') }}"></script>
        <script src="{{ asset('/js/admin/core/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.query-object.js') }}"></script>
        <script src="{{ asset('/js/admin/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
        <!--  Notifications Plugin    -->
        <script src="{{ asset('/js/admin/plugins/bootstrap-notify.js') }}"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="{{ asset('/js/admin/paper-dashboard.min.js?v=2.0.0') }}" type="text/javascript"></script>
        <script>
            $(document).ready(function(){
            	setTimeout(function(){
	            	$(".status-alert").fadeOut();
            	}, 3000);
            });
        </script>
        @yield('page-scripts')
    </body>
</html>