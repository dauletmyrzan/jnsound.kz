<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900" rel="stylesheet">

    <!-- Styles -->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/themify/themify-icons.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/elegant-font/html-css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/slick/slick.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/noui/nouislider.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/lightbox2/css/lightbox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/util.css?v=2') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css?v=2.1') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css?v=2') }}">
    @yield('style')
</head>
<body class="animsition">
	<a href="https://api.whatsapp.com/send?phone=77772826992&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5%20%F0%9F%99%82%21" target="_blank">
		<button class="button-widget-open bigEntrance"></button>
	</a>
	@if(!Request::is('/'))
    @include('parts.header')
    @endif
    <main class="min-height-500">
        @yield('content')
    </main>
    @include('parts.footer')
    <!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>
	<div id="dropDownSelect2"></div>

    <script type="text/javascript" src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <script>
    	Array.prototype.remove = function() {
		    var what, a = arguments, L = a.length, ax;
		    while (L && this.length) {
		        what = a[--L];
		        while ((ax = this.indexOf(what)) !== -1) {
		            this.splice(ax, 1);
		        }
		    }
		    return this;
		};
    </script>
	<script type="text/javascript" src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/select2/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/slick/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/slick-custom.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/countdowntime/countdowntime.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/lightbox2/js/lightbox.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/noui/nouislider.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.query-object.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("body").on("click", ".swal-button--confirm", function(){
				window.location.reload();
			});
			$('.block2-btn-addcart button').on("click", function(){
				var nameProduct = $(this).data("title");
				var product_id = $(this).data("id");
				var price = $(this).data("price");
				$.ajax({
					url: "{{ route('cart.store') }}",
					type: "post",
					data: {
						_token: "{{ csrf_token() }}",
						id: product_id,
						name: nameProduct,
						quantity: 1,
						price: price
					},
					success: function(response){
						if(response['status'] == 'ok'){
							swal(nameProduct, " добавлена в корзину!", "success");
						}else{
							swal("Ошибка", "Ошибка при добавлении товара, обратитесь к администратору сайта.", "error");
						}
					}
				});
			});

			$('.block2-btn-addwishlist').each(function(){
				var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
				$(this).on('click', function(){
					swal(nameProduct, " добавлена в избранное!", "success");
				});
			});
			/*[ No ui ]
			===========================================================*/
			var filterBar = document.getElementById('filter-bar');

			noUiSlider.create(filterBar, {
				start: [ {{ isset($_GET['low_price']) ? intval($_GET['low_price']) : 5000 }}, {{ isset($_GET['high_price']) ? intval($_GET['high_price']) : 300000 }} ],
				connect: true,
				step: 1000,
				range: {
					'min': 0,
					'max': 500000
				}
			});

			var skipValues = [
				document.getElementById('value-lower'),
				document.getElementById('value-upper')
			];

			filterBar.noUiSlider.on('update', function( values, handle ) {
				skipValues[handle].innerHTML = Math.round(values[handle]) ;
			});
			$("#apply_price").on("click", function(){
				var from = skipValues[0].innerHTML;
				var to = skipValues[1].innerHTML;
				window.location.href = window.location.origin + window.location.pathname + "?low_price=" + from + "&high_price=" + to;
			});
		});
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect2')
		});
	</script>
	@yield('page-scripts')
</body>
</html>
