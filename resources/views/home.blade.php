@extends('layouts.app')

@section('content')
    <!-- Slide1 -->
    <?php /*
    <section class="slide1">
		<div class="wrap-slick1">
			<div class="slick1">
                @foreach($banners as $key => $banner)
				<div class="item-slick1 item{{ $banner->key }}-slick1" style="background-image: url(images/banners/{{ $banner->img_src }});">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<span class="caption1-slide1 m-text1 t-center animated visible-false m-b-15" data-appear="fadeInDown">
                            {{ $banner->title }}
						</span>

						<h2 class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="fadeInUp">
							{{ $banner->description }}
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
							<!-- Button -->
							<a href="{{ $banner->link }}" class="flex-c-m size2 bo-rad-23 s-text2 bggray hov1 trans-0-4">
								Посмотреть
							</a>
						</div>
					</div>
				</div>
                @endforeach
			</div>
		</div>
	</section>
	*/?>
	<section id="main" class="hover-top" style="background-image: url({{ asset('images/wallpaper.jpg') }})">
		<div class="container">
			<div id="b_head">
				<!-- Header desktop -->
		        <div class="container-menu-header1 wrap_header_desktop">
		            <div class="topbar">
		            	<div class="container">
			                <div class="wrap_menu">
			                    <nav class="menu">
			                        <ul class="main_menu">
			                            @foreach($categories as $cat)
			                            <li>
			                                <a href="/category/{{ $cat->code }}">{{ $cat->name }}</a>
			                            </li>
			                            @endforeach
			                            <form action="/search/">
						                    <input type="text" name="q" class="main-search-input" placeholder="Поиск...">
			                            </form>
			                            <span class="topbar-email ml-3">
					                        info@jnsound.kz
					                    </span>
			                        </ul>
			                    </nav>
			                </div>
		               </div>
		            </div>

		            <div class="wrap_header">
		                <!-- Logo -->
		                <a href="/" class="logo">
		                    <img src="{{ asset('images/logo.png') }}" alt="JNSound.kz">
		                </a>

		                <!-- Header Icon -->
		                <div class="header-icons">
		                	<div class="tel">
		                		<a href="tel:+77772826992" class="num mango-calltracking">+7 (777) 282-6992</a>
		                	</div>
		                    <div class="header-wrapicon2">
		                        <a href="{{ route('cart.index') }}">
		                            <img src="{{ asset('images/icons/shopping-cart-black-shape.png') }}" class="header-icon1 js-show-header-dropdown" alt="JNSound.kz">
		                            <span class="header-icons-noti">{{ Cart::content()->count() }}</span>
		                        </a>
		                    </div>
		                </div>
		            </div>
		        </div>

		        <!-- Header Mobile -->
		        <div class="wrap_header_mobile">
		            <!-- Logo moblie -->
		            <a href="index.php" class="logo-mobile">
		                <img src="{{ asset('images/logo.png') }}" alt="JNSound.kz">
		            </a>

		            <!-- Button show menu -->
		            <div class="btn-show-menu">
		                <!-- Header Icon mobile -->
		                <div class="header-icons-mobile">
		                    <div class="header-wrapicon2">
		                        <a href="{{ route('cart.index') }}">
		                            <img src="{{ asset('images/icons/shopping-cart-black-shape.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">
		                            <span class="header-icons-noti">{{ Cart::content()->count() }}</span>
		                        </a>
		                    </div>
		                </div>

		                <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
		                    <span class="hamburger-box">
		                        <span class="hamburger-inner"></span>
		                    </span>
		                </div>
		            </div>
		            <div class="tel">
                		<a href="tel:+77772826992" class="num mango-calltracking">+7 (777) 282-6992</a>
                		<form action="/search/" class="mt-3">
		                    <input type="text" style="width: 100%;" name="q" class="main-search-input" placeholder="Поиск...">
                        </form>
                	</div>
		        </div>

		        <!-- Menu Mobile -->
		        <div class="wrap-side-menu" >
		            <nav class="side-menu">
		                <ul class="main-menu">
		                    @foreach($categories as $cat)
		                    <li class="item-menu-mobile">
		                        <a href="/category/{{ $cat->code }}">{{ $cat->name }}</a>
		                    </li>
		                    @endforeach
		                </ul>
		            </nav>
		        </div>
			</div>
			<h1 class="offer">МАГНИТОЛЫ, КОЛОНКИ, УСИЛИТЕЛИ<br>ОТ ПРЯМЫХ ПОСТАВЩИКОВ</h1>
			<h2 class="offer-2">Оптом и в розницу</h2>
			<!-- features -->
			<section id="features" class="features bggray p-t-62 p-b-46">
				<div class="flex-w p-l-15 p-r-15">
					<div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon1">
						<img src="{{ asset('images/icons/shipping.png') }}" width="50">
						<h4 class="m-text14 t-center mt-4">
							Доставка в день заказа (Алматы)
						</h4>
					</div>

					<div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon2">
						<img src="{{ asset('images/icons/like.png') }}" width="50">
						<h4 class="m-text14 t-center mt-4">
							100% гарантия
						</h4>
					</div>

					<div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon2">
						<img src="{{ asset('images/icons/shipping.png') }}" width="50">
						<h4 class="m-text14 t-center mt-4">
							Доставка по всему Казахстану
						</h4>
					</div>
				</div>
			</section>
		</div>
	</section>

	<!-- Categories -->
	<section class="banner bggray p-t-40 p-b-40">
		<div class="container">
			<div class="row">
				@foreach($categories as $cat)
				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<div class="block1 hov-img-zoom pos-relative m-b-30">
                        <img style='background-color: whitesmoke;' src="images/{{ $cat->img_src != '' ? $cat->img_src : 'gallery-01.jpg' }}" alt="{{ $cat->name }}">

                        <div class="block1-wrapbtn w-size12">
                            <!-- Button -->
                            <a href="/category/{{ $cat->code }}" class="flex-c-m size2 s-text3 bg4 hov1 trans-0-4" style="color:#fff;text-align:center;line-height:16px;">
                                {{ $cat->name }}
                            </a>
                        </div>
                    </div>
                </div>
				@endforeach
			</div>
		</div>
	</section>

	<!-- Products -->
	<section class="newproduct bggray p-t-45 p-b-50">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Наши товары
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
                    @foreach($products as $product)
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew1">
								<img src="{{ isset($product->pictures[0]->img_src) ? 'images/products/'.$product->pictures[0]->img_src : 'images/item-cart-01.jpg' }}" alt="{{ $product->title }}">
								<div class="block2-overlay trans-0-4" onclick="window.location.href='/product/{{ $product->id }}'">
									<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
										<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
										<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
									</a>
								</div>
							</div>

							<div class="block2-txt p-t-20 pl-2 pr-2">
								<a href="/product/{{ $product->id }}" class="block2-name dis-block s-text3 p-b-5">
                                    {{ $product->title }}
								</a>
								<span class="block2-price m-text6 p-r-5">
                                    <strong>{{ number_format($product->price, 0, '.', ' ') }} ₸</strong>
								</span>
								@if($product->old_price != '' && $product->old_price != 0)
								<span class="block2-price block2-old-price m-text6 p-r-5">
                                    {{ number_format($product->old_price, 0, '.', ' ') }} ₸
								</span>
								@endif
							</div>
							<div class="block2-btn-addcart w-size1 mt-3">
								<button data-id="{{ $product->id }}" data-title="{{ $product->title }}" data-price="{{ $product->price }}" class="flex-c-m size1 bg1 s-text1 hov1">
									Купить
								</button>
							</div>
						</div>
					</div>
                    @endforeach
				</div>
			</div>

		</div>
	</section>

    <?/*
	<!-- Banner2 -->
	<section class="banner2 bg5 p-t-55 p-b-55">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-8 col-lg-6 m-l-r-auto p-t-15 p-b-15">
					<div class="hov-img-zoom pos-relative">
						<img src="images/banner-08.jpg" alt="IMG-BANNER">

						<div class="ab-t-l sizefull flex-col-c-m p-l-15 p-r-15">
							<span class="m-text9 p-t-45 fs-20-sm">
								The Beauty
							</span>

							<h3 class="l-text1 fs-35-sm">
								Lookbook
							</h3>

							<a href="#" class="s-text4 hov2 p-t-20 ">
								View Collection
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-8 col-lg-6 m-l-r-auto p-t-15 p-b-15">
					<div class="bggray hov-img-zoom pos-relative p-b-20per-ssm">
						<img src="images/shop-item-09.jpg" alt="IMG-BANNER">

						<div class="ab-t-l sizefull flex-col-c-b p-l-15 p-r-15 p-b-20">
							<div class="t-center">
								<a href="product-detail.html" class="dis-block s-text3 p-b-5">
									Gafas sol Hawkers one
								</a>

								<span class="block2-oldprice m-text7 p-r-5">
									$29.50
								</span>

								<span class="block2-newprice m-text8">
									$15.90
								</span>
							</div>

							<div class="flex-c-m p-t-44 p-t-30-xl">
								<div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
									<span class="m-text10 p-b-1 days">
										69
									</span>

									<span class="s-text5">
										дн.
									</span>
								</div>

								<div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
									<span class="m-text10 p-b-1 hours">
										04
									</span>

									<span class="s-text5">
										час.
									</span>
								</div>

								<div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
									<span class="m-text10 p-b-1 minutes">
										32
									</span>

									<span class="s-text5">
										мин.
									</span>
								</div>

								<div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
									<span class="m-text10 p-b-1 seconds">
										05
									</span>

									<span class="s-text5">
										сек.
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    */?>
	<?php
	/*
    <hr>
	<!-- Blog -->
	<section class="blog bggray p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					Новости
				</h3>
			</div>

			<div class="row">
                @foreach($news as $news_single)
                    <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
                        <!-- Block3 -->
                        <div class="block3">
                            <a href="blog-detail.html" class="block3-img dis-block hov-img-zoom">
                                <img src="images/{{ $news_single->img_src }}" alt="{{ $news_single->title }}">
                            </a>

                            <div class="block3-txt p-t-14">
                                <h4 class="p-b-7">
                                    <a href="blog-detail.html" class="m-text11">
                                        {{ $news_single->title }}
                                    </a>
                                </h4>
                                
                                <span class="s-text7">{{ date("d.m.Y", strtotime($news_single->created_at)) }}</span>

                                <p class="s-text8 p-t-16">
                                    {{ $news_single->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
			</div>
		</div>
	</section>
	*/?>

	<!-- Instagram -->
	<?php /*
	<section class="instagram p-t-20">
		<div class="sec-title p-b-52 p-l-15 p-r-15">
			<h3 class="m-text5 t-center">
				@ Подпишись на нас в Instagram
			</h3>
		</div>

		<div class="flex-w">
			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="images/gallery-03.jpg" alt="IMG-INSTAGRAM">

				<a href="#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="images/gallery-07.jpg" alt="IMG-INSTAGRAM">

				<a href="#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="images/gallery-09.jpg" alt="IMG-INSTAGRAM">

				<a href="#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="images/gallery-13.jpg" alt="IMG-INSTAGRAM">

				<a href="#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="images/gallery-15.jpg" alt="IMG-INSTAGRAM">

				<a href="#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>
		</div>
	</section>
	*/?>
@endsection
