@extends('layouts.app')

@section('content')
    <!-- Title Page -->
	<section class="bg-title-page p-t-30 p-b-30 flex-col-c-m">
		<h2 class="l-text1 t-center" style="color: #222;">
            {{ $category->name }}
		</h2>
    </section>
    
    <!-- Content page -->
	<section class="bggray products-section p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-8 col-lg-9 p-b-50 order-md-12">
					@php $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'default' @endphp
					<div class="p-b-35">
						<div class="text-right">
							<span style="color: gray; margin-right: 10px">Сортировать по</span>
							<select name="sorting" class="form-control" style="display: inline-block; width: auto;">
								<option value="default">умолчанию</option>
								<option value="popular" {{ $sort_by == 'popular' ? 'selected' : '' }}>популярности</option>
								<option value="price_asc" {{ $sort_by == 'price_asc' ? 'selected' : '' }}>возрастанию цены</option>
								<option value="price_desc" {{ $sort_by == 'price_desc' ? 'selected' : '' }}>убыванию цены</option>
							</select>
						</div>
					</div>

					<!-- Product -->
					<div class="row">
                        @foreach($products as $product)
						<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
							<div class="block2 product-div">
								<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew1">
									<img src="/{{ isset($product->pictures[0]) ? 'images/products/'.$product->pictures[0]->img_src : 'images/no-image.svg' }}" alt="{{ $product->title }}">
									<div class="block2-overlay trans-0-4" onclick="window.location.href='/product/{{ $product->id }}'">
										<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
											<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
											<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
										</a>
									</div>
								</div>
								<div class="block2-txt p-t-20">
									<a href="/product/{{ $product->id }}" class="block2-name dis-block s-text3 p-b-5">
										{{ $product->title }}
									</a>
									<span class="block2-price p-r-5">
										<strong>{{ number_format($product->price, 0, '.', ' ') }} ₸</strong>
									</span>
									@if($product->old_price != '' && $product->old_price != 0)
									<span class="block2-price block2-old-price m-text6 p-r-5">
			                            {{ number_format($product->old_price, 0, '.', ' ') }} ₸
									</span>
									@endif
								</div>
								<div class="block2-btn-addcart w-size1 pt-4">
									<button data-id="{{ $product->id }}" data-title="{{ $product->title }}" data-price="{{ $product->price }}" class="flex-c-m size1 bg4 hov1 s-text1">
										В корзину
									</button>
								</div>
							</div>
						</div>
                        @endforeach
					</div>
                    <!-- Pagination -->
					{{ $products->links() }}
				</div>
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-20" id="filters">
					<div class="leftbar p-r-20 p-r-0-sm">
						<h4 class="m-text14 p-b-7">
							Категории
						</h4>
						<ul class="p-b-54">
                            @foreach($categories as $cat)
							<li class="p-t-4">
								<a href="/category/{{ $cat->code }}" class="s-text13" {!! $cat->code == $category->code ? 'style="font-weight: bold;"' : '' !!}>
									{{ $cat->name }}
								</a>
							</li>
                            @endforeach
						</ul>
						<h4 class="m-text14 p-b-7">
							Бренд
						</h4>
						@php
						$selected_brands = array();
						if(isset($_GET['brand']))
						{
							$selected_brands = explode(",", $_GET['brand']);
						}
						@endphp
						<ul class="p-b-54">
                            @foreach($brands as $brand)
							<li class="p-t-4">
								<input type="checkbox" class="brand-checkbox" id="brand{{ $brand->id }}" name="brand" value="{{ $brand->id }}" {{ in_array($brand->id, $selected_brands) ? 'checked' : '' }}>
								<label for="brand{{ $brand->id }}">{{ $brand->name }}</label>
							</li>
                            @endforeach
						</ul>
						<div class="filter-price p-t-10 p-b-30 bo3">
							<div class="m-text15 p-b-17">
								Цена
							</div>

							<div class="wra-filter-bar">
								<div id="filter-bar"></div>
							</div>

							<div class="flex-sb-m flex-w p-t-16">
								<div class="w-size11">
									<!-- Button -->
									<button class="flex-c-m size4 bg7 bo-rad-15 hov1 s-text14 trans-0-4" id="apply_price">
										Ок
									</button>
								</div>

								<div class="s-text3 p-t-10 p-b-10">
									<span id="value-lower">1000</span> - <span id="value-upper">300000</span> тг
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$("select[name='sorting']").on('change', function(){
			var sort_by = $(this).val();
			window.location.href = $.query.SET("sort_by", sort_by);
		});
		$(".brand-checkbox").on("change", function(){
			$el = $(this);
			if($el.is(':checked')){
				var current_brands = $.query.get('brand').toString();
				if(current_brands != ""){
					current_brands = current_brands.split(",");
				}else{
					current_brands = [];
				}
				var val = $el.val();
				if(!current_brands.includes(val)){
					current_brands.push(val);
				}
				var newString = current_brands.join(",");
				if(current_brands.length == 1){
					newString = val;
				}
				window.location.href = $.query.SET("brand", newString);
			}else{
				var current_brands = $.query.get('brand').toString();
				current_brands = current_brands.split(",");
				var val = $el.val();
				current_brands.remove(val);
				if(current_brands.length == 0){
					if($.query.REMOVE('brand') == ""){
						window.location.href = location.pathname;
					}
					else{
						window.location.href = $.query.REMOVE("brand");
					}
				}else{
					window.location.href = $.query.SET("brand", current_brands.join(','));
				}
			}
		});
	});
</script>
@endsection