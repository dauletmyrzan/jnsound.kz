@extends('layouts.app')

@section('style')
<style>
	h3{
		font-family: sans-serif;
		font-weight: bold;
	}
	.contact{
		font-family: sans-serif;
		font-size: 15pt;
		margin: 10px 0;
		color: #000;
	}
</style>
@endsection

@section('content')
	<section class="bg-title-page p-t-20 p-b-20 flex-col-c-m">
		<h2 class="l-text1 t-center" style="color:#222">
			Контакты
		</h2>
    </section>
    
    <!-- Content page -->
	<section class="bgwhite p-t-20 p-b-20" style="min-height: 500px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3>Телефон</h3>
					<p class="contact">+7(777) 282-6992</p>
					<hr>
					<h3>E-mail</h3>
					<p class="contact">leon.podkorytov@gmail.com</p>
				</div>
				<div class="col-md-8" id="map_holder">
					<a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940000870610/center/76.90459728240968,43.25087628443941/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/firm/9429940000870610/photos/9429940000870610/center/76.90459728240968,43.25087628443941/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.904606,43.250581/zoom/16/routeTab/rsType/bus/to/76.904606,43.250581╎ALMA-TV, сеть торгово-сервисных центров?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до ALMA-TV, сеть торгово-сервисных центров</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":"100%","height":500,"borderColor":"#a3a3a3","pos":{"lat":43.25087628443941,"lon":76.90459728240968,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"9429940000870610"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
				</div>
			</div>
		</div>
	</section>
@endsection