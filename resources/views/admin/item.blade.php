@extends('layouts.admin')

@section('breadcrumbs')
    <a href="{{ route('admin.items') }}" class="btn btn-primary"><span class="nc-icon nc-minimal-left"></span> Назад</a>
@endsection

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2>{{ $item->title }}</h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h3>Информация</h3>
                        	<form action="{{ route('item.update_info') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ $item->id }}">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" class="form-control" name="title" value="{{ $item->title }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="active" id="active" value="1" {{ $item->active == 1 ? 'checked' : '' }}>
                                    <label for="active">Активность</label>
                                </div>
                                <div class="form-group">
                                    <label>Описание</label>
                                    <textarea class="form-control" style="min-height: 150px;" name="description" required>{!! $item->description !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Цена</label>
                                    <input type="number" class="form-control" step="1" min="0" name="price" value="{{ $item->price }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Старая цена</label>
                                    <input type="number" class="form-control" step="1" min="0" name="old_price" value="{{ $item->old_price }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Бренд</label>
                                    <select class="form-control" name="brand_id" value="{{ $item->brand_id }}">
                                        @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Категория (для множественного выбора зажмите CTRL)</label>
                                    <select name="category_id[]" style="min-height: 150px;" class="form-control" multiple required>
                                        @foreach($categories as $cat)
                                        <option value="{{ $cat->id }}" {{ array_key_exists($cat->id, $item_categories) ? 'selected' : '' }}>{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Сортировка (чем меньше значение, тем товар выходит первее)</label>
                                    <input type="number" class="form-control" min="0" step="1" name="sort_number" required value="{{ $item->sort_number }}">
                                </div>
                                <button class="btn btn-success">Применить</button>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <h3>Картинки</h3>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('item.update_pictures') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="product_id" value="{{ $item->id }}">
                                <div class="form-group">
                                    @foreach($item->pictures as $pic)
                                    <div style="display:inline-block;">
                                        <div class="admin-item-img" style="border:4px solid #eee;background-image:url('{{ asset('/images/products/' . $pic->img_src) }}')"></div><br><br>
                                        <input type="checkbox" id="pic{{ $pic->id }}" name="remove_pictures[]" value="{{ $pic->id }}"> <label for="pic{{ $pic->id }}">Удалить картинку</label>
                                    </div>
                                    @endforeach
                                </div>
                                <div>
                                    <label>Загрузить новые картинки</label>
                                    <input style="width:40%" type="file" class="form-control" multiple name="pictures[]">
                                </div>
                                <button class="btn btn-success">Применить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-danger">Опасная зона</h2>
                </div>
                <div class="card-body">
                    <p>Внимание! Удалив товар, вы уже не сможете его восстановить. Вместе с товаром удалятся и его картинки.</p>
                    <form action="{{ route('item.delete') }}" method="POST" id="remove_item_form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $item->id }}">
                        <button class="btn btn-danger">Удалить товар</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
        $("#remove_item_form").find("button").on("click", function(){
            if(confirm('Вы действительно хотите удалить товар?')){
                $("#remove_item_form").submit();
            }
            return false;
        });
	});
</script>
@endsection