@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новый баннер</h2>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                	<form action="{{ route('banner.store') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                		<div class="form-group">
                			<label for="title">Заголовок</label>
	                		<input type="text" id="title" class="form-control" name="title" required>
                		</div>
                		<div class="form-group">
	                		<input type="checkbox" id="active" name="active" value="1" checked>
                			<label for="active">Активность</label>
                		</div>
                		<div class="form-group">
                			<label for="description">Описание</label>
	                		<textarea id="description" class="form-control" name="description" required></textarea>
                		</div>
                		<div class="form-group">
                			<label for="link">Ссылка</label>
	                		<input type="text" id="link" placeholder="https://" class="form-control" name="link" required>
                		</div>
                		<div class="form-group">
                			<label for="sort_number">Сортировка</label>
	                		<input type="number" min="0" step="1" id="sort_number" class="form-control" name="sort_number" required>
                		</div>
                		<div>
                			<label for="picture">Картинка</label>
                			<input type="file" class="form-control" name="picture" id="picture" required>
                		</div>
						<button class="mt-3 btn btn-success">Добавить</button>
						<a href="{{ route('admin.banners') }}" class="mt-3 btn btn-primary">Отмена</a>
                	</form>
                </div>
            </div>
        </div>
    </div>
@endsection