@extends('layouts.admin')

@section('breadcrumbs')
    <a href="{{ route('admin.banners') }}" class="btn btn-primary"><span class="nc-icon nc-minimal-left"></span> Назад</a>
@endsection

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2>{{ $banner->title }}</h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h3>Информация</h3>
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        	<form action="{{ route('banner.update_info') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $banner->id }}">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" class="form-control" name="title" value="{{ $banner->title }}" required>
                                </div>
                                <div class="form-group">
                                	<input type="checkbox" name="active" id="active" value="1" {{ $banner->active == 1 ? 'checked' : '' }}>
                                	<label for="active">Активность</label>
                                </div>
                                <div class="form-group">
                                    <label>Описание</label>
                                    <textarea class="form-control" style="min-height: 150px;" name="description" required>{!! $banner->description !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Ссылка</label>
                                    <input type="text" class="form-control" name="link" value="{{ $banner->link }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Сортировка (чем меньше значение, тем товар выходит первее)</label>
                                    <input type="number" class="form-control" min="0" step="1" name="sort_number" required value="{{ $banner->sort_number }}">
                                </div>
                                <div class="form-group">
                                    <div class="admin-item-img" style="width:100%;border:4px solid #eee;background-image:url('{{ asset('/images/banners/' . $banner->img_src) }}')"></div>
                                </div>
                                <div>
                                    <label>Загрузить новую картинку</label>
                                    <input type="file" class="form-control" multiple name="picture">
                                </div><br>
                                <button class="btn btn-success">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-danger">Опасная зона</h2>
                </div>
                <div class="card-body">
                    <p>Внимание! Удалив баннер, вы уже не сможете его восстановить. Вместе с баннером удалится и его картинка.</p>
                    <form action="{{ route('banner.delete') }}" method="POST" id="remove_banner_form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $banner->id }}">
                        <button class="btn btn-danger">Удалить баннер</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
        $("#remove_banner_form").find("button").on("click", function(){
            if(confirm('Вы действительно хотите удалить баннер?')){
                $("#remove_banner_form").submit();
            }
            return false;
        });
	});
</script>
@endsection