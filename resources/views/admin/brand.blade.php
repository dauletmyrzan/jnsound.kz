@extends('layouts.admin')

@section('breadcrumbs')
    <a href="{{ route('admin.brands') }}" class="btn btn-primary"><span class="nc-icon nc-minimal-left"></span> Назад</a>
@endsection

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2>{{ $brand->title }}</h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h3>Информация</h3>
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        	<form action="{{ route('brand.update_info') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $brand->id }}">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" class="form-control" name="name" value="{{ $brand->name }}" required>
                                </div>
                                <div class="form-group">
                                	<input type="checkbox" name="active" id="active" value="1" {{ $brand->active == 1 ? 'checked' : '' }}>
                                	<label for="active">Активность</label>
                                </div>
                                <br>
                                <button class="btn btn-success">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-danger">Опасная зона</h2>
                </div>
                <div class="card-body">
                    <p>Внимание! Удалив бренд, вы уже не сможете его восстановить.</p>
                    <form action="{{ route('brand.delete') }}" method="POST" id="remove_brand_form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $brand->id }}">
                        <button class="btn btn-danger">Удалить бренд</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
        $("#remove_brand_form").find("button").on("click", function(){
            if(confirm('Вы действительно хотите удалить бренд?')){
                $("#remove_brand_form").submit();
            }
            return false;
        });
	});
</script>
@endsection