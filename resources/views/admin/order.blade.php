@extends('layouts.admin')

@section('breadcrumbs')
    <a href="../" class="btn btn-primary"><span class="nc-icon nc-minimal-left"></span> Назад</a>
@endsection


@section('content')
    @if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header ">
                    <h5 class="card-title">Заказ № {{ $order->id }}</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('order.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $order->id }}">
                        <table class="table">
                            <tr>
                                <td>Имя:</td>
                                <td><b>{{ $order->name }}</b></td>
                            </tr>
                            <tr>
                                <td>Телефон:</td>
                                <td><b>{{ $order->phone }}</b></td>
                            </tr>
                            <tr>
                                <td>Адрес:</td>
                                <td><b>{{ $order->address }}</b></td>
                            </tr>
                            <tr>
                                <td>Дата заказа:</td>
                                <td><b>{{ $order->created_at }}</b></td>
                            </tr>
                            <tr>
                                <td>Статус:</td>
                                <td>
                                    <select name="status_id" class="form-control" onchange="document.getElementById('save_btn').style.display='block'">
                                        @foreach($statuses as $status)
                                        <option value="{{ $status->id }}" {{ $status->name == $order->status ? 'selected' : '' }}>{{ $status->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Итого:</td>
                                <td style="font-size: 16pt;"><b>{{ $order->total_price }} ₸</b></td>
                            </tr>
                        </table>
                        <button id="save_btn" class="btn btn-success" style="display:none;">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ">
                    <h5 class="card-title">Товары</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Количество</th>
                            </thead>
                            <tbody>
                                @foreach($order_items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ $app->make('url')->to('/') }}/product/{{ $item->product_id }}" target="_blank">{{ $item->title }}</a></td>
                                    <td>{{ $item->price }} ₸</td>
                                    <td>{{ $item->quantity }} шт.</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-center">
                    <form action="{{ route('order.delete') }}" method="POST" id="remove_order_form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $order->id }}">
                        <button class="btn btn-danger">Удалить заказ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("#remove_order_form").find("button").on("click", function(){
            if(confirm('Вы действительно хотите удалить заказ?')){
                $("#remove_order_form").submit();
            }
            return false;
        });
    });
</script>
@endsection