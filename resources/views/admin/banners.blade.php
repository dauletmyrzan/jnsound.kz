@extends('layouts.admin')

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Баннеры</h2>
            <a href="{{ route('admin.new_banner') }}" class="btn btn-primary">Добавить баннер</a><br><br>
            <div class="card">
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            	<th>#</th>
                            	<th>Активность</th>
                                <th>Заголовок</th>
                                <th>Картинка</th>
                                <th>Ссылка</th>
                                <th>Дата создания</th>
                            </thead>
                            <tbody>
                            	@foreach($banners as $key => $banner)
								<tr class="banner-row cursor-pointer" onclick="window.location.href='/admin/banner/{{ $banner->id }}'">
									<td>{{ $key+1 }}</td>
									<td>{!! $banner->active ? '<span class="nc-icon nc-check-2"></span>' : '<span class="nc-icon nc-simple-remove"></span>' !!}</td>
									<td>{{ $banner->title }}</td>
									<td><img src="/images/banners/{{ $banner->img_src }}" width="100"></td>
									<td>{{ $banner->link }}</td>
									<td>{{ $banner->created_at }}</td>
								</tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Modal -->
	<div class="modal fade" id="categoryEditModal" tabindex="-1" role="dialog" aria-labelledby="categoryEditModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="categoryEditModalLabel">Изменение категории</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
            	<form action="{{ route('category.update') }}" method="POST" enctype="multipart/form-data">
		            <div class="modal-body">
	            		@csrf
	            		<input type="hidden" name="id">
	            		<div class="form-group">
	            			<label>Название</label>
	            			<input type="text" class="form-control" name="name" required>
	            		</div>
	            		<div class="form-group">
	            			<label>Код</label>
	            			<input type="text" class="form-control" name="code" required>
	            		</div>
	            		<div>
	            			<label>Картинка</label><br>
	            			<img src="" class="category-img" width="50%"><br>
	            			<input type="file" class="form-control" name="picture">
	            		</div>
		            </div>
		            <div class="modal-footer">
	            		<button class="btn btn-success">Сохранить</button>
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
		            </div>
            	</form>
	        </div>
	    </div>
	</div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$(".category-row").click(function(){
			var id = $(this).data("id");
			var name = $(this).find(".category-name").text();
			var code = $(this).find(".category-code").text();
			var picture = $(this).find(".category-picture").text();
			$("input[name='id']").val(id);
			$("input[name='name']").val(name);
			$("input[name='code']").val(code);
			$(".category-img").attr("src", window.location.origin + "/images/" + picture);
			$("#categoryEditModal").modal();
		});
	});
</script>
@endsection