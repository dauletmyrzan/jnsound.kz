@extends('layouts.admin')

@section('content')
    @if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новый товар</h2>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                	<form action="{{ route('item.store') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                		<div class="form-group">
                			<label for="title">Название</label>
	                		<input type="text" id="title" class="form-control" name="title" required>
                		</div>
                		<div class="form-group">
	                		<input type="checkbox" id="active" name="active" value="1" checked>
                			<label for="active">Активность</label>
                		</div>
                		<div class="form-group">
                			<label for="description">Описание</label>
	                		<textarea id="description" class="form-control" name="description" required></textarea>
                		</div>
                        <div class="form-group">
                            <label for="price">Цена</label>
                            <input type="number" min="0" step="1" id="price" class="form-control" name="price" required>
                        </div>
                        <div class="form-group">
                            <label for="old_price">Старая цена</label>
                            <input type="number" min="0" step="1" id="old_price" class="form-control" name="old_price">
                        </div>
                		<div class="form-group">
                			<label for="brand">Бренд</label>
                            <select name="brand_id" class="form-control" id="brand_id">
                                @foreach($brands as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                		</div>
                        <div class="form-group">
                            <label for="category_id">Категории</label>
                            <select name="category_id[]" class="form-control" style="min-height: 140px;" required id="category_id" multiple>
                                @foreach($categories as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                		<div class="form-group">
                			<label for="sort_number">Сортировка</label>
	                		<input type="number" min="0" step="1" id="sort_number" class="form-control" name="sort_number" required>
                		</div>
                		<div>
                			<label for="pictures">Загрузите картинки</label>
                			<input type="file" class="form-control" name="pictures[]" id="pictures" required multiple>
                            <p class="text-muted my-2" style="font-size: 9pt;"><span class="nc-icon nc-alert-circle-i"></span> Для сохранения эстетичности сайта загрузите квадратные изображения.</p>
                		</div>
						<button class="mt-3 btn btn-success">Добавить</button>
						<a href="{{ route('admin.items') }}" class="mt-3 btn btn-primary">Отмена</a>
                	</form>
                </div>
            </div>
        </div>
    </div>
@endsection