@extends('layouts.admin')

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2>Категории</h2>
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            	<th>ID</th>
                                <th>Название</th>
                                <th>Картинка</th>
                                <th>Код</th>
                            </thead>
                            <tbody>
                            	@foreach($categories as $cat)
								<tr class="category-row" data-id="{{ $cat->id }}">
									<td class="category-id">{{ $cat->id }}</td>
									<td class="category-name">{{ $cat->name }}</td>
									<td class="category-picture">{{ $cat->img_src }}</td>
									<td class="category-code">{{ $cat->code }}</td>
								</tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Modal -->
	<div class="modal fade" id="categoryEditModal" tabindex="-1" role="dialog" aria-labelledby="categoryEditModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="categoryEditModalLabel">Изменение категории</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
            	<form action="{{ route('category.update') }}" method="POST" enctype="multipart/form-data">
		            <div class="modal-body">
	            		@csrf
	            		<input type="hidden" name="id">
	            		<div class="form-group">
	            			<label>Название</label>
	            			<input type="text" class="form-control" name="name" required>
	            		</div>
	            		<div class="form-group">
	            			<label>Код</label>
	            			<input type="text" class="form-control" name="code" required>
	            		</div>
	            		<div>
	            			<label>Картинка</label><br>
	            			<img src="" class="category-img" width="50%"><br>
	            			<input type="file" class="form-control" name="picture">
	            		</div>
		            </div>
		            <div class="modal-footer">
	            		<button class="btn btn-success">Сохранить</button>
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
		            </div>
            	</form>
	        </div>
	    </div>
	</div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$(".category-row").click(function(){
			var id = $(this).data("id");
			var name = $(this).find(".category-name").text();
			var code = $(this).find(".category-code").text();
			var picture = $(this).find(".category-picture").text();
			$("input[name='id']").val(id);
			$("input[name='name']").val(name);
			$("input[name='code']").val(code);
			$(".category-img").attr("src", window.location.origin + "/images/" + picture);
			$("#categoryEditModal").modal();
		});
	});
</script>
@endsection