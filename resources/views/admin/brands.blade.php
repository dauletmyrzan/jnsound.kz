@extends('layouts.admin')

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Бренды</h2>
            <a href="{{ route('admin.new_brand') }}" class="btn btn-primary">Добавить бренд</a><br><br>
            <div class="card">
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            	<th>#</th>
                            	<th>Активность</th>
                                <th>Название</th>
                                <th>Дата создания</th>
                            </thead>
                            <tbody>
                            	@foreach($brands as $key => $brand)
								<tr class="brand-row cursor-pointer" onclick="window.location.href='/admin/brand/{{ $brand->id }}'">
									<td>{{ $key+1 }}</td>
									<td>{!! $brand->active ? '<span class="nc-icon nc-check-2"></span>' : '<span class="nc-icon nc-simple-remove"></span>' !!}</td>
									<td>{{ $brand->name }}</td>
									<td>{{ $brand->created_at }}</td>
								</tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
	});
</script>
@endsection