@extends('layouts.admin')

@section('content')
	@if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Товары</h2>
            <a href="{{ route('admin.new_item') }}" class="btn btn-primary">Добавить товар</a><br><br>
            <div class="card">
            	<div class="card-header">
                    <div class="row">
                        <div class="col-md-3 col-lg-2">
                            <label>Категория</label>
                            <select name="category_id" class="form-control">
                                <option value="all">Все</option>
                                @foreach($categories as $cat)
                                <option value="{{ $cat->id }}" {{ (isset($_GET['category']) && $cat->id == $_GET['category']) ? 'selected' : '' }}>{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            	<th style="width: 40px;">ID</th>
                                <th style="width: 80px;">Активность</th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Старая цена</th>
                                <th>Дата создания</th>
                            </thead>
                            <tbody>
                            	@foreach($items as $item)
								<tr class="item-row cursor-pointer" data-id="{{ $item->id }}" onclick="window.location.href='/admin/item/{{$item->id}}'">
									<td class="item-id">{{ $item->id }}</td>
                                    <td>{!! $item->active ? '<span class="nc-icon nc-check-2"></span>' : '<span class="nc-icon nc-simple-remove"></span>' !!}</td>
                                    <td class="item-name">{{ $item->title }}</td>
                                    <td class="item-picture">{{ $item->price }} ₸</td>
									<td class="item-picture">{{ $item->old_price }} ₸</td>
									<td class="item-code">{{ $item->created_at }}</td>
								</tr>
                            	@endforeach
                            </tbody>
                        </table>
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$("select[name='category_id']").on("change", function(){
            var query = $.query.SET('category', $(this).val());
            window.location.href = query;
        });
	});
</script>
@endsection