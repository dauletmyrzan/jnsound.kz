@extends('layouts.admin')

@section('content')
    @if(session('status') && session('status'))
        <div class="status-alert alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
            {{ session('status') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <h2>Заявки</h2>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3 col-lg-2">
                            <label>Статус</label>
                            <select name="status_id" class="form-control">
                                <option value="all">Все</option>
                                @foreach($statuses as $status)
                                <option value="{{ $status->id }}" {{ (isset($_GET['status']) && $status->id == $_GET['status']) ? 'selected' : '' }}>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Номер заказа</th>
                                <th>Имя</th>
                                <th>Телефон</th>
                                <th>Дата</th>
                                <th>Статус</th>
                            </thead>
                            <tbody>
                            	@foreach($orders as $order)
                                <tr>
                                	<td><a href="/admin/order/{{ $order->id }}">{{ $order->id }}</a></td>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->phone }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>{{ $order->status }}</td>
                                    <td><a href="/admin/order/{{ $order->id }}" class="btn btn-primary">Посмотреть заказ</a></td>
                                </tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("select[name='status_id']").on("change", function(){
            var query = $.query.SET('status', $(this).val());
            window.location.href = query;
        });
    });
</script>
@endsection