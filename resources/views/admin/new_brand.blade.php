@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новый бренд</h2>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                	<form action="{{ route('brand.store') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                		<div class="form-group">
                			<label for="name">Название</label>
	                		<input type="text" id="name" class="form-control" name="name" required>
                		</div>
                		<div class="form-group">
	                		<input type="checkbox" id="active" name="active" value="1" checked>
                			<label for="active">Активность</label>
                		</div>
						<button class="mt-3 btn btn-success">Добавить</button>
						<a href="{{ route('admin.brands') }}" class="mt-3 btn btn-primary">Отмена</a>
                	</form>
                </div>
            </div>
        </div>
    </div>
@endsection