@extends('layouts.app')

@section('content')
	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
        <!-- breadcrumb -->
        <div class="bread-crumb bgwhite flex-w p-r-15 p-t-30 p-l-15-sm">
            <a href="index.html" class="s-text16">
                Главная
                <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
            </a>

            <a href="/category/{{ $product->categories[0]->code }}" class="s-text16">
                {{ $product->categories[0]->name }}
                <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
            </a>

            <span class="s-text17">
                {{ $product->title }}
            </span>
        </div>
		<div class="flex-w flex-sb product">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>

					<div class="slick3">
						@foreach($product->pictures as $picture)
							<div class="item-slick3" data-thumb="{{ asset('images/products/' . $picture->img_src) }}">
								<div class="wrap-pic-w">
									<img src="{{ asset('images/products/' . $picture->img_src) }}" alt="{{ $product->title }}">
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
                    {{ $product->title }}
				</h4>

				<span class="ml-4 m-text17">
					<strong>{{ number_format($product->price, 0, '.', ' ') }} ₸</strong>
				</span>

				@if($product->old_price != '' && $product->old_price != 0)
				<span class="block2-old-price m-text17 p-r-5" style="float: none;">
                    {{ number_format($product->old_price, 0, '.', ' ') }} ₸
				</span>
				@endif

				<div class="p-t-33 p-b-60">
					<div class="flex-w p-t-10">
						<div class="flex-m flex-w">
							<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>

								<input class="size8 m-text18 t-center num-product" type="number" name="num-product" value="1">

								<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>

							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								<button id="add_cart" class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
									В корзину
								</button>
							</div>
						</div>
					</div>
				</div>

				<div class="p-b-45">
					<span class="s-text8">Категории: <strong>{!! $product->categories_text !!}</strong></span>
				</div>

				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Описание
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
                            {!! nl2br($product->description) !!}
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>


	@if(count($similar_products) > 0)
	<!-- Relate Product -->
	<section class="relateproduct bgwhite p-t-45 p-b-138">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Похожие товары
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
                    @foreach($similar_products as $similar_product)
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img src="{{ isset($similar_product->pictures[0]) ? asset('images/products/' . $similar_product->pictures[0]->img_src) : 'images/item-03.jpg' }}">
								<div class="block2-overlay trans-0-4" onclick="window.location.href='/product/{{ $similar_product->id }}'">
									<div class="block2-btn-addcart w-size1 trans-0-4">
										<!-- Button -->
										<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
                                            В корзину
										</button>
									</div>
								</div>
							</div>
							<div class="block2-txt p-t-20">
								<a href="/product/{{ $similar_product->id }}" class="block2-name dis-block s-text3 p-b-5">
                                    {{ $similar_product->title }}
								</a>
								<span class="block2-price m-text6 p-r-5">
									<strong>{{ number_format($similar_product->price, 0, '.', ' ') }} ₸</strong>
								</span>
								@if($similar_product->old_price != '' && $similar_product->old_price != 0)
								<span class="block2-price block2-old-price m-text6 p-r-5">
		                            {{ number_format($similar_product->old_price, 0, '.', ' ') }} ₸
								</span>
								@endif
							</div>
						</div>
					</div>
                    @endforeach
				</div>
			</div>
		</div>
	</section>
	@endif
@endsection

@section('page-scripts')
<script>
	$(document).ready(function(){
		$("#add_cart").on("click", function(){
			var nameProduct = "{{ $product->title }}";
			var product_id = {{ $product->id }};
			$.ajax({
				url: "{{ route('cart.store') }}",
				type: "post",
				data: {
					_token: "{{ csrf_token() }}",
					id: product_id,
					name: nameProduct,
					quantity: $("input[name='num-product']").val(),
					price: {{ $product->price }}
				},
				success: function(response){
					if(response['status'] == 'ok'){
						swal(nameProduct, " добавлена в корзину!", "success");
					}else{
						swal("Ошибка", "Ошибка при добавлении товара, обратитесь к администратору сайта.", "error");
					}
				}
			});
		});
	});
</script>
@endsection