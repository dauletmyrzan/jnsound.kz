@extends('layouts.app')

@php
$categories = App\Category::all();
@endphp

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            На ваш почтовый ящик было отправлено письмо с ссылкой на страницу восстановления пароля.
                        </div>
                    @endif

                    Если вы не получили письмо, щелкните <a href="{{ route('verification.resend') }}">сюда</a> и мы отправим вам заново.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
