<!-- Footer -->
<footer class="bg6 p-t-25 p-b-25 p-l-45 p-r-45">
    <div class="container">
        <div class="flex-w">
            <div class="w-size-quarter p-t-30 p-l-15 p-r-15 respon3">
                <h4 class="s-text12 p-b-30">
                    Свяжитесь с нами
                </h4>
                <div>
                    <p class="s-text7 w-size27" style="text-indent: 0;">Возникли вопросы?<br> Напишите нам, мы доступны 24/7<br><a href="mailto:info@jnsound.kz">info@jnsound.kz</a></p>
                    <div class="flex-m p-t-30">
                        <a href="https://www.instagram.com/jnsound.kz/" target="_blank" class="topbar-social-item fa fa-instagram" style="padding-left: 0;"> <span style="font-size:10pt;vertical-align:center;font-family:'Roboto';">Instagram</span></a>
                    </div>
                </div>
            </div>

            <div class="w-size-quarter p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    Категории
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="/category/automagnitols" class="s-text7">
                            Автомагнитолы
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="/category/subwoofers" class="s-text7">
                            Сабвуферы
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="/category/security" class="s-text7">
                            Сигнализации
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="/category/accessories" class="s-text7">
                            Аксессуары
                        </a>
                    </li>
                </ul>
            </div>

            <div class="w-size-quarter p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    Ссылки
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="{{ route('about') }}" class="s-text7">
                            О магазине
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="{{ route('contact') }}" class="s-text7">
                            Контакты
                        </a>
                    </li>
                </ul>
            </div>
            <div class="w-size-quarter p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    Подпишись на новости
                </h4>

                <form action="{{ route('subscribe') }}" method="post">
                    @csrf
                    <div class="effect1 w-size9">
                        <input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="Введите ваш email">
                        <span class="effect1-line"></span>
                    </div>

                    <div class="w-size2 p-t-20">
                        <!-- Button -->
                        <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                            Подписаться
                        </button>
                    </div>

                </form>
                @if(session('status') && session('status'))
                    <div class="status-alert alert alert-success" style="margin:20px auto;text-align:center;">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
        <hr>

        <div class="t-center p-l-15 p-r-15">
            <img src="{{ asset('images/logo.png') }}" alt="JNSound.kz" width="70">
            <div class="t-center s-text8 p-t-20">
                © 2018 Все права защищены. | JNSound.kz</a>
            </div>
        </div>
    </div>
</footer>