<!-- Header -->
<header class="header1" style="padding: 0 10px;">
    <!-- Header desktop -->
    <div class="wrap_header_desktop container">
        <div class="container-menu-header1">
            <div class="topbar" style="padding-top: 15px;">
                <div class="container">
                    <div class="wrap_menu">
                        <nav class="menu">
                            <ul class="main_menu">
                                @foreach($categories as $cat)
                                <li>
                                    <a href="/category/{{ $cat->code }}">{{ $cat->name }}</a>
                                </li>
                                @endforeach
                                <form action="/search/">
                                    <input type="text" name="q" class="main-search-input" placeholder="Поиск...">
                                </form>
                            </ul>
                        </nav>
                    </div>
               </div>
            </div>

            <div class="wrap_header">
                <!-- Logo -->
                <a href="/" class="logo">
                    <img src="{{ asset('images/logo.png') }}" alt="JNSound.kz">
                </a>

                <!-- Header Icon -->
                <div class="header-icons">
                    <div class="tel">
                        <a href="tel:+74951499638" class="num mango-calltracking">+7 (777) 282-6992</a>
                    </div>
                    <div class="header-wrapicon2">
                        <a href="{{ route('cart.index') }}">
                            <img src="{{ asset('images/icons/shopping-cart-black-shape.png') }}" class="header-icon1 js-show-header-dropdown" alt="JNSound.kz">
                            <span class="header-icons-noti">{{ Cart::content()->count() }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="/" class="logo-mobile">
            <img src="{{ asset('images/logo.png') }}" alt="JNSound.kz">
        </a>

        <!-- Button show menu -->
        <div class="btn-show-menu">
            <!-- Header Icon mobile -->
            <div class="header-icons-mobile">
                <div class="header-wrapicon2">
                    <a href="{{ route('cart.index') }}">
                        <img src="{{ asset('images/icons/shopping-cart-black-shape.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti">{{ Cart::content()->count() }}</span>
                    </a>
                </div>
            </div>

            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </div>
        </div>
    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu" >
        <nav class="side-menu">
            <ul class="main-menu">
                @foreach($categories as $cat)
                <li class="item-menu-mobile">
                    <a href="/category/{{ $cat->code }}">{{ $cat->name }}</a>
                </li>
                @endforeach
            </ul>
        </nav>
    </div>
</header>