@extends('layouts.app')

@section('style')
<style>
	p{
		line-height: 25pt;
		font-size: 12pt;
		color: #222;
		margin-bottom: 20px;
	}
</style>
@endsection

@section('content')
	<section class="bg-title-page p-t-20 p-b-20 flex-col-c-m">
		<h2 class="l-text1 t-center" style="color:#222">
			О магазине
		</h2>
    </section>
    
    <!-- Content page -->
	<section class="bgwhite p-t-20 p-b-20">
		<div class="container">
			<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>
		</div>
	</section>
@endsection