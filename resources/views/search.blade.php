@extends('layouts.app')
@section('content')
<div class="container">
	<h3 class="mt-5 mb-4">Результат поиска по запросу «{{ $_GET['q'] }}»</h3>
	<hr>
	<div class="row">
		@foreach($products as $product)
			<div class="col-sm-12 col-md-3 col-lg-3 p-b-50">
				<div class="block2 product-div">
					<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew1">
						<img src="/{{ isset($product->pictures[0]) ? 'images/products/'.$product->pictures[0]->img_src : 'images/no-image.svg' }}" alt="{{ $product->title }}">
						<div class="block2-overlay trans-0-4" onclick="window.location.href='/product/{{ $product->id }}'"></div>
					</div>
					<div class="block2-txt p-t-20">
						<a href="/product/{{ $product->id }}" class="block2-name dis-block s-text3 p-b-5">
							{{ $product->title }}
						</a>
						<span class="block2-price p-r-5">
							<strong>{{ number_format($product->price, 0, '.', ' ') }} ₸</strong>
						</span>
						@if($product->old_price != '' && $product->old_price != 0)
						<span class="block2-price block2-old-price m-text6 p-r-5">
                            {{ number_format($product->old_price, 0, '.', ' ') }} ₸
						</span>
						@endif
					</div>
					<div class="block2-btn-addcart w-size1 mt-3">
						<button data-id="{{ $product->id }}" data-title="{{ $product->title }}" data-price="{{ $product->price }}" class="flex-c-m size1 bg4 hov1 s-text1">
							В корзину
						</button>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
@endsection