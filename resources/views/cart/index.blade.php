@extends('layouts.app')

@section('content')
	@if(count($cart_items) == 0)
		<div class="p-t-70 p-b-100" style="min-height: 50vh">
			<h1 class="t-center mt-5">Корзина пуста</h1>
			@if(session('status') && session('status'))
			    <div class="alert alert-success" style="width:500px;margin:20px auto;text-align:center;">
			        {{ session('status') }}
			    </div>
			@endif
		</div>
	@else
	<!-- Title Page -->
	<h1 class="t-center mt-5">Корзина</h1>

	<!-- Cart -->
	<section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
			<!-- Cart item -->
			<div class="container-table-cart pos-relative">
				<div class="wrap-table-shopping-cart bgwhite">
					<table class="table-shopping-cart">
						<tr class="table-head">
							<th class="column-1"></th>
							<th class="column-2">Товар</th>
							<th class="column-3">Цена</th>
						</tr>
						@foreach($cart_items as $item)
						<tr class="table-row">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="{{ isset($item->images[0]) ? 'images/products/'.$item->images[0]->img_src : 'images/no-image.svg' }}" alt="{{ $item->images[0]->img_src ?? 'images/no-image.svg' }}">
									<form action="{{ route('cart.remove') }}" method="POST">
										@csrf
										<input type="hidden" name="row_id" value="{{ $item->rowId }}">
										<button style="display:block;margin:10px auto;" class="btn btn-danger btn-sm bo-rad-23 remove-from-cart">Удалить</button>
									</form>
								</div>
							</td>
							<td class="column-2"><a href="/product/{{ $item->id }}" target="_blank">{{ $item->name }}</td>
							<td class="column-3">{{ $item->price }} ₸</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
			<!-- Total -->
			<div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
				<h5 class="m-text20 p-b-24">
					Данные заказа
				</h5>
				<form action="{{ route('cart.request') }}" method="POST">
					@csrf
					<div class="flex-w flex-sb bo10 p-t-15 p-b-20">
						<div class="w-full">
							<div class="size1 bo4 m-b-12">
								<input class="sizefull s-text7 p-l-15 p-r-15" required type="text" name="name" placeholder="Имя">
							</div>
							<div class="size1 bo4 m-b-12">
								<input class="sizefull s-text7 p-l-15 p-r-15" required type="text" name="phone" placeholder="Телефон">
							</div>
						</div>
						<div class="w-full">
							<div class="size1 bo4 m-b-22">
								<input class="sizefull s-text7 p-l-15 p-r-15" required type="text" name="address" placeholder="Адрес">
							</div>
						</div>
					</div>

					<!--  -->
					<div class="flex-w flex-sb-m p-t-26 p-b-30">
						<span class="m-text22 w-size19 w-full-sm">
							Итого:
						</span>

						<span id="total_product_price" class="m-text21 w-size20 w-full-sm">
							{{ $cart_items->total }} ₸
						</span>
						<label class="mt-3 text-muted fs-14">Оплата при получении товара курьеру.</label>
					</div>

					<div class="size15 trans-0-4">
						<!-- Button -->
						<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Оформить заказ
						</button>
					</div>
				</form>
			</div>
		</div>
	</section>
	@endif

	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
	});
</script>
@endsection