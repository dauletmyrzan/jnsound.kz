<?php

namespace App\Http\Middleware;
use Closure;

class OnlyAjax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if(!$request->ajax())
        {
            return response("Forbidden. You don't have permission to access this page on this server.", 403);
        }
        return $next($request);
    }
}
