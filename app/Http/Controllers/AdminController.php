<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Order;
use App\Status;
use App\Product;
use App\Picture;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function orders(Request $request)
    {
    	$orders = Order::paginate(20);
    	if(isset($request->status) && $request->status != 'all')
    	{
    		$orders = Order::where('status_id', intval($request->status))->paginate(20);
    	}
    	$statuses = Status::all();
    	foreach($orders as &$o)
    	{
    		$o->status = Status::find($o->status_id)->name;
    	}
    	return view('admin.orders', compact(['orders', 'statuses']));
    }
    public function order(Request $request)
    {
    	$order = Order::find($request->id);
        if(!isset($order))
        {
            abort(404);
        }
   		$order->status = Status::find($order->status_id)->name;
   		$order_items = Order::getOrderItems($order->id);
   		$statuses = Status::all();
    	return view('admin.order', compact(['order', 'order_items', 'statuses']));
    }
    public function updateOrderStatus(Request $request)
    {
    	$order = Order::find($request->id);
    	$order->status_id = $request->status_id;
    	$order->save();
    	return redirect()->back()->with('status', 'Статус заказа успешно обновлен!');
    }
    public function showCategories()
    {
    	$categories = Category::all();
    	return view('admin.categories', compact(['categories']));
    }
    public function updateCategory(Request $request)
    {
    	$category = Category::find($request->id);
    	$category->name = $request->name;
    	$category->code = $request->code;
    	if($request->hasFile('picture'))
    	{
    		$extension = $request->file('picture')->extension();
    		Storage::disk('public_uploads')->putFileAs('/images', $request->file('picture'), $request->code . "." . $extension);
	    	$category->img_src = $request->code . "." . $extension;
    	}
    	$category->save();
    	return redirect()->back()->with('status', 'Категория успешно обновлена!');
    }
    public function items(Request $request)
    {
    	$category = Category::find($request->category);
    	$categories = Category::all();
    	$items = Product::paginate(15);
    	if(isset($request->category) && $request->category != 'all')
    	{
    		$items = $category->products()->paginate(15);
    	}
    	return view('admin.items', compact(['items', 'categories']));
    }
    public function item(Request $request)
    {
    	$item = Product::find($request->id);
    	if(!isset($item))
    	{
    		return redirect()->route('admin.items');
    	}
        $categories = Category::all();
    	$brands = Brand::all();
    	$item_categories = array();
    	foreach($item->categories as $cat)
    	{
    		$item_categories[$cat->id] = $cat->name;
    	}
    	return view('admin.item', compact(['item', 'categories', 'item_categories', 'brands']));
    }
}
