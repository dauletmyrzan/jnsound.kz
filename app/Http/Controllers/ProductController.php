<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Picture;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('admin.new_item', compact(['categories', 'brands']));
    }

    public function store(Request $request)
    {
        $product = new Product;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->active = isset($request->active) && intval($request->active) == 1 ? true : false;
        $product->price = $request->price;
        $product->old_price = $request->old_price;
        $product->brand_id = $request->brand_id;
        $product->sort_number = $request->sort_number;
        $categories_arr = array();
        foreach($request->category_id as $cat_id)
        {
            $categories_arr[] = intval($cat_id);
        }
        $product->save();
        $product->categories()->attach($categories_arr);
        if($request->hasFile('pictures'))
        {
            $this->validate($request, [
                'pictures[]' => 'mimes:jpeg,png,bmp,tiff,gif|max:4096',
            ],
            $messages = [
                'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max' => 'Размер файла великоват. Надо до 4 Мб.'
            ]);
            foreach($request->file('pictures') as $pic)
            {
                $extension = $pic->extension();
                $hash = md5($pic->getClientOriginalName().rand(0, 10000));
                Storage::disk('public_uploads')->putFileAs('/images/products/', $pic, $hash . "." . $extension);
                $picture = new Picture;
                $picture->img_src = $hash . "." . $extension;
                $picture->product_id = $product->id;
                $picture->save();
            }
        }
        return redirect()->route('admin.items')->with('status', 'Товар успешно добавлен!');
    }

    public function showCategory(Request $request)
    {
        $category = Category::where('code', $request->code)->first();
        $categories = Category::all();
        $brands = Brand::all();
        $low_price = isset($request->low_price) ? intval($request->low_price) : 0;
        $high_price = isset($request->high_price) ? intval($request->high_price) : 500000;
        $currentPage = $request->page ?? 1;
        $sort_by = $request->sort_by ?? 'default';
        if($sort_by == 'default')
        {
            $sort_by = 'title';
            $sort_order = 'asc';
        }
        elseif($sort_by == 'popular')
        {
            $sort_by = 'sort_number';
            $sort_order = 'asc';
        }
        elseif($sort_by == 'price_asc')
        {
            $sort_by = 'price';
            $sort_order = 'asc';
        }
        else
        {
            $sort_by = 'price';
            $sort_order = 'desc';
        }
        $brand_ids = explode(",", $request->brand) ?? array();
        $perPage = 12;
        $products = $category->products()->
                    where('price', '>=', $low_price)->
                    where('price', '<=', $high_price)->
                    whereIn('brand_id', $brand_ids)->
                    orderBy($sort_by, $sort_order)->
                    paginate($perPage);
        return view('category', compact(['category', 'categories', 'products', 'brands']));
    }
    public function showProduct(Request $request)
    {
        $id = isset($request->id) ? $request->id : null;
        if($id == null)
        {
            return abort(404);
        }
        $product = Product::where('active', 1)->where('id', '=', $id)->first();
        if(!isset($product))
        {
            return abort(404);
        }
        $categories = $product->categories;
        $product->categories_text = "";
        for($i = 0; $i < count($categories); $i++)
        {
            if($i != 0)
            {
                $product->categories_text .= ", ";
            }
            $product->categories_text .= "<a class='btn-link' style='color:#007bff' href='/category/" . $categories[$i]->code . "'>" . $categories[$i]->name . "</a>";
        }
        $category_id = $product->categories[0]->id;
        $category = Category::find($category_id);
        $similar_products = $category->products;
        for($i = 0; $i < count($similar_products); $i++)
        {
            if($similar_products[$i]->id == $id)
            {
                unset($similar_products[$i]);
            }
        }
        $categories = Category::all();
        $brands = Brand::all();
        return view('product', compact(['product', 'similar_products', 'categories', 'brands']));
    }
    public function updateItemPictures(Request $request)
    {
        if(isset($request->remove_pictures))
        {
            Picture::destroy($request->remove_pictures);
        }
        if($request->hasFile('pictures'))
        {
            $this->validate($request, [
                'pictures[]' => 'mimes:jpeg,png,bmp,tiff,gif |max:4096',
            ],
            $messages = [
                'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max' => 'Размер файла великоват. Надо до 4 Мб.'
            ]);
            foreach($request->file('pictures') as $pic)
            {
                $extension = $pic->extension();
                $hash = md5($pic->getClientOriginalName().rand(0, 10000));
                Storage::disk('public_uploads')->putFileAs('/images/products/', $pic, $hash . "." . $extension);
                $picture = new Picture;
                $picture->img_src = $hash . "." . $extension;
                $picture->product_id = $request->product_id;
                $picture->save();
            }
        }
        return redirect()->back()->with('status', 'Картинки успешно обновлены!');
    }
    public function updateItemInfo(Request $request)
    {
        $product = Product::find($request->id);
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->old_price = $request->old_price;
        $product->brand_id = $request->brand_id;
        $product->sort_number = $request->sort_number;
        $product->active = isset($request->active) && intval($request->active) == 1 ? true : false;
        $categories_arr = array();
        foreach($request->category_id as $cat_id)
        {
            $categories_arr[] = intval($cat_id);
        }
        $product->categories()->detach();
        $product->categories()->attach($categories_arr);
        $product->save();
        return redirect()->back()->with('status', 'Информация по товару успешно обновлена!');
    }
    public function delete(Request $request)
    {
        $product = Product::find($request->id);
        $pictures = $product->pictures;
        foreach($pictures as $pic)
        {
            Storage::disk('public_uploads')->delete("/images/products/".$pic->img_src);
        }
        $product->delete();
        return redirect()->route('admin.items')->with('status', 'Товар успешно удален!');
    }
}
