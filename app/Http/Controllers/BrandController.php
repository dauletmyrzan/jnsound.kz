<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
    	$brands = Brand::all();
    	return view('admin.brands', compact(['brands']));
    }

    public function create()
    {
    	return view('admin.new_brand');
    }

    public function store(Request $request)
    {
    	$brand = new Brand;
        $brand->name = $request->name;
        $brand->active = isset($request->active) && intval($request->active) == 1 ? true : false;
        $brand->save();
        return redirect()->route('admin.brands')->with('status', 'Бренд успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $brand = Brand::find($request->id);
        return view('admin.brand', compact(['brand']));
    }

    public function update(Request $request)
    {
        $brand = Brand::find($request->id);
        $brand->name = $request->name;
        $brand->active = isset($request->active) && intval($request->active) == 1 ? true : false;
        $brand->save();
        return redirect()->back()->with('status', 'Бренд успешно обновлен!');
    }

    public function destroy(Request $request)
    {
        $brand = Brand::find($request->id);
        $brand->delete();
        return redirect()->route('admin.brands')->with('status', 'Баннер успешно удален!');
    }
}
