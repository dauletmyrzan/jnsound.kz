<?php

namespace App\Http\Controllers;

use App\News;
use App\Banner;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::where('active', 1)->orderBy('sort_number', 'asc')->get();
        $products = Product::where('active', 1)->orderBy('created_at', 'desc')->take(8)->get();
        $categories = Category::all();
        $news = News::where('active', 1)->orderBy('created_at', 'desc')->take(3)->get();
        return view('home', compact(['banners', 'categories', 'products', 'news']));
    }
}
