<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
	public function index()
	{
		$cart_items = Cart::content();
		$cart_items->total = Cart::total();
		foreach($cart_items as $key => &$item)
		{
			$item->images = Product::find($item->id)->pictures;
		}
        $categories = Category::all();
		return view('cart.index', compact(['cart_items', 'category', 'categories']));
	}
    public static function store(Request $request)
    {
    	if(Cart::add($request->id, $request->name, intval($request->quantity), intval($request->price))->associate('App\Product'))
    	{
	    	return response()->json([
	    		'status' => 'ok'
	    	]);
    	}
    	return response()->json([
    		'status' => 'error'
    	]);
    }
    public function destroy()
    {
    	Cart::destroy();
    	return response()->json([
    		'status' => 'ok'
    	]);
    }
    public function remove(Request $request)
    {
    	Cart::remove($request->row_id);
    	return redirect()->back();
    }
    public function request(Request $request)
    {
    	$cart_items = Cart::content();
    	$order = new Order;
    	$order->name = $request->name;
    	$order->phone = $request->phone;
    	$order->address = $request->address;
    	$order->status_id = 1;
    	$order->total_price = intval(Cart::total(0, '', ''));
    	$order->save();
    	$order_items_arr = array();
    	foreach($cart_items as $item)
    	{
    		$order_items_arr[] = [
    			'product_id' => $item->id,
    			'title' => $item->name,
    			'quantity' => $item->qty,
    			'price' => $item->price,
    			'order_id' => $order->id
    		];
    	}
    	if(!DB::table('order_items')->insert($order_items_arr))
    	{
	    	return redirect()->back()->with('status', 'Возникла ошибка, попробуйте позднее.');
    	}
    	Cart::destroy();
    	return redirect()->back()->with('status', 'Спасибо за заказ! Мы вам скоро позвоним :)');
    }
}
