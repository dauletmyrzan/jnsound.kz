<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
    	$query = preg_replace("/[^A-Za-zА-Яа-я0-9?!]/", "", $request->q);
    	$products = Product::where('title', 'LIKE', '%' . $query . '%')->orWhere('description', 'LIKE', '%' . $query . '%')->get();
    	$categories = Category::all();
    	return view('search', compact(['products', 'categories']));
    }
}
