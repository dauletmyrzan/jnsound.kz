<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $banner = Order::find($request->id);
        $banner->delete();
        return redirect()->route('admin.orders')->with('status', 'Заказ успешно удален!');
    }
}
