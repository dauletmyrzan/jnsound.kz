<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscribeController extends Controller
{
    public function subscribe(Request $request)
    {
    	$email = $request->email;
    	$email_have = DB::table('subscribers')->select('email')->get();
    	if(!$email_have->contains('email', $email))
    	{
    		$msg = 'Вы подписались!';
	    	DB::table('subscribers')->insert(
	    		['email' => $email]
			);
    	}
    	else
    	{
    		$msg = 'Спасибо, вы уже подписаны!';
    	}
		return redirect()->back()->with('status', $msg);
    }
}
