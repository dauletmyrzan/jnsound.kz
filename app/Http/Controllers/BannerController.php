<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('sort_number', 'asc')->get();
        return view('admin.banners', compact(['banners']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new_banner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = new Banner;
        $banner->title = $request->title;
        $banner->active = isset($request->active) && intval($request->active) == 1 ? true : false;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->sort_number = $request->sort_number;
        if($request->hasFile('picture'))
        {
            $this->validate($request, [
                'picture' => 'mimes:jpeg,png,bmp,tiff,gif |max:4096',
            ],
            $messages = [
                'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max' => 'Размер файла великоват. Надо до 4 Мб.'
            ]);
            $pic = $request->file('picture');
            $extension = $pic->extension();
            $hash = md5($pic->getClientOriginalName().rand(0, 10000));
            Storage::disk('public_uploads')->putFileAs('/images/banners/', $pic, $hash . "." . $extension);
            $banner->img_src = $hash . "." . $extension;
        }
        $banner->save();
        return redirect()->route('admin.banners')->with('status', 'Баннер успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $banner = Banner::find($request->id);
        return view('admin.banner', compact(['banner']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function edit(Banners $banners)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $banner = Banner::find($request->id);
        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->sort_number = $request->sort_number;
        $banner->active = isset($request->active) && intval($request->active) == 1 ? true : false;
        if($request->hasFile('picture'))
        {
            $this->validate($request, [
                'picture' => 'mimes:jpeg,png,bmp,tiff,gif |max:4096',
            ],
            $messages = [
                'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max' => 'Размер файла великоват. Надо до 4 Мб.'
            ]);
            $pic = $request->file('picture');
            $extension = $pic->extension();
            $hash = md5($pic->getClientOriginalName().rand(0, 10000));
            Storage::disk('public_uploads')->delete("/images/banners/".$banner->img_src);
            Storage::disk('public_uploads')->putFileAs('/images/banners/', $pic, $hash . "." . $extension);
            $banner->img_src = $hash . "." . $extension;
        }
        $banner->save();
        return redirect()->back()->with('status', 'Баннер успешно обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $banner = Banner::find($request->id);
        Storage::disk('public_uploads')->delete("/images/banners/".$banner->img_src);
        $banner->delete();
        return redirect()->route('admin.banners')->with('status', 'Баннер успешно удален!');
    }
}
