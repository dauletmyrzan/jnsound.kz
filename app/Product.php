<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Get the pictures for the product.
     */
    public function pictures()
    {
        return $this->hasMany('App\Picture');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
