<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public static function getOrderItems($id)
    {
    	return DB::table("order_items")->select("id", "title", "price", "quantity", "product_id")->where("order_id", $id)->get();
    }
}
