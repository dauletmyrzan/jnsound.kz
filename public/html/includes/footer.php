<!-- Footer -->
<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
            <h4 class="s-text12 p-b-30">
                Свяжитесь с нами
            </h4>

            <div>
                <p class="s-text7 w-size27">
                    Возникли вопросы? Напишите нам, мы доступны 24/7.<br>info@jnsound.kz
                </p>
                <div class="flex-m p-t-30">
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-facebook"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-instagram"></a>
                </div>
            </div>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
            <h4 class="s-text12 p-b-30">
                Категории
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Автомагнитолы
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Сабвуферы
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Сигнализации
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Аксессуары
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
            <h4 class="s-text12 p-b-30">
                Ссылки
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Поиск
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="about.php" class="s-text7">
                        О магазине
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="contact.php" class="s-text7">
                        Контакты
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="news.php" class="s-text7">
                        Новости
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
            <h4 class="s-text12 p-b-30">
                Помощь
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Отслеживание заказа
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Возврат товара
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="shipping.php" class="s-text7">
                        Доставка
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="faq.php" class="s-text7">
                        FAQ
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
            <h4 class="s-text12 p-b-30">
                Подпишись на новости
            </h4>

            <form>
                <div class="effect1 w-size9">
                    <input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="Введите ваш email">
                    <span class="effect1-line"></span>
                </div>

                <div class="w-size2 p-t-20">
                    <!-- Button -->
                    <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                        Подписаться
                    </button>
                </div>

            </form>
        </div>
    </div>
    <hr>

    <div class="t-center p-l-15 p-r-15">
        <img src="images/logo.png" alt="JNSound.kz" width="70">
        <div class="t-center s-text8 p-t-20">
            © <?php echo date("Y")?> Все права защищены. | JNSound.kz</a>
        </div>
    </div>
</footer>