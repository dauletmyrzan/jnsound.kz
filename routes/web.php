<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('/home', function(){
    return redirect('/');
})->name('home');

Route::get('/catalog', function(){
    $categories = App\Category::all();
    if(count($categories) > 0)
    {
        $category = $categories->get(0);
        return redirect("/category/" . $category->code);
    }
    else
    {
        abort(404);
    }
})->name('catalog');

Route::get('/category/{code}', [
    'uses' => 'ProductController@showCategory'
]);

/* 
 * ===================
 * CART
 * ===================
 */
Route::get('/cart', [
    'as' => 'cart.index',
    'uses' => 'CartController@index'
]);

Route::post('/store', [
    'as' => 'cart.store',
    'uses' => 'CartController@store',
    'middleware' => 'ajax'
]);

Route::post('/cart.remove', [
    'as' => 'cart.remove',
    'uses' => 'CartController@remove'
]);

Route::post('/cart.request', [
    'as' => 'cart.request',
    'uses' => 'CartController@request'
]);

/* 
 * ===================
 * Regular pages
 * ===================
 */

Route::get('/news', [
    'as' => 'news',
    'uses' => 'NewsController@showNews'
]);

Route::get('/about', function(){
    $categories = App\Category::all();
    return view('about')->with('categories', $categories);
})->name('about');

Route::get('/contact', function(){
    $categories = App\Category::all();
    return view('contact')->with('categories', $categories);
})->name('contact');

Route::get('/tracking', function(){
    return view('tracking');
})->name('tracking');

Route::get('/return', function(){
    return view('return');
})->name('return');

Route::get('/shipping', function(){
    return view('shipping');
})->name('shipping');

Route::get('/faq', function(){
    return view('faq');
})->name('faq');

Route::get('/search', 'SearchController@search');

/* 
 * ===================
 * PRODUCT
 * ===================
 */
Route::get('/product/{id}', [
    'uses' => 'ProductController@showProduct'
]);

/*
 * ===================
 * ADMIN
 * ===================
 */
Route::get('/admin', [
    'as' => 'admin.orders',
    'uses' => 'AdminController@orders',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/order/{id}', [
    'uses' => 'AdminController@order',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/categories', [
    'as' => 'admin.categories',
    'uses' => 'AdminController@showCategories',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/items', [
    'as' => 'admin.items',
    'uses' => 'AdminController@items',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/item/{id}', [
    'uses' => 'AdminController@item',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/new_item', [
    'as' => 'admin.new_item',
    'uses' => 'ProductController@create',
    'middleware' => ['auth', 'admin']
]);

/*
 * ===================
 * ADMIN POST ROUTES
 * ===================
 */
Route::post('/order.update', [
    'as' => 'order.update',
    'uses' => 'AdminController@updateOrderStatus',
    'middleware' => ['auth', 'admin']
]);

Route::post('/category.update', [

    'as' => 'category.update',
    'uses' => 'AdminController@updateCategory',
    'middleware' => ['auth', 'admin']
]);

Route::post('/item.update_info', [
    'as' => 'item.update_info',
    'uses' => 'ProductController@updateItemInfo',
    'middleware' => ['auth', 'admin']
]);

Route::post('/item.update_pictures', [
    'as' => 'item.update_pictures',
    'uses' => 'ProductController@updateItemPictures',
    'middleware' => ['auth', 'admin']
]);

Route::post('/item.delete', [
    'as' => 'item.delete',
    'uses' => 'ProductController@delete',
    'middleware' => ['auth', 'admin']
]);

/**
 * ===================
 * BRANDS
 * ===================
 */

Route::get('/admin/brands', [
    'as' => 'admin.brands',
    'uses' => 'BrandController@index',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/brand/{id}', [
    'uses' => 'BrandController@show',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/new_brand', [
    'as' => 'admin.new_brand',
    'uses' => 'BrandController@create',
    'middleware' => ['auth', 'admin']
]);

Route::post('/brand.update_info', [
    'as' => 'brand.update_info',
    'uses' => 'BrandController@update',
    'middleware' => ['auth', 'admin']
]);

Route::post('/brand.delete', [
    'as' => 'brand.delete',
    'uses' => 'BrandController@destroy',
    'middleware' => ['auth', 'admin']
]);

Route::post('/brand.store', [
    'as' => 'brand.store',
    'uses' => 'BrandController@store',
    'middleware' => ['auth', 'admin']
]);

/**
 * ===================
 * BANNERS
 * ===================
 */

Route::get('/admin/banners', [
    'as' => 'admin.banners',
    'uses' => 'BannerController@index',
    'middleware' => ['auth', 'admin']
]);


Route::get('/admin/banner/{id}', [
    'uses' => 'BannerController@show',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/new_banner', [
    'as' => 'admin.new_banner',
    'uses' => 'BannerController@create',
    'middleware' => ['auth', 'admin']
]);

Route::post('/banner.update_info', [
    'as' => 'banner.update_info',
    'uses' => 'BannerController@update',
    'middleware' => ['auth', 'admin']
]);

Route::post('/banner.delete', [
    'as' => 'banner.delete',
    'uses' => 'BannerController@destroy',
    'middleware' => ['auth', 'admin']
]);

Route::post('/banner.store', [
    'as' => 'banner.store',
    'uses' => 'BannerController@store',
    'middleware' => ['auth', 'admin']
]);


/**
 * ==================
 * OTHER STUFF
 * ==================
 */

Route::post('/order.delete', [
    'as' => 'order.delete',
    'uses' => 'OrderController@destroy',
    'middleware' => ['auth', 'admin']
]);

Route::post('/item.store', [
    'as' => 'item.store',
    'uses' => 'ProductController@store',
    'middleware' => ['auth', 'admin']
]);

Route::post('subscribe', [
    'as' => 'subscribe',
    'uses' => 'SubscribeController@subscribe',
]);