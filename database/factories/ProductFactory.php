<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'price' => $faker->numberBetween($min = 1000, $max = 9000),
        'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'sort_number' => $faker->numberBetween($min = 1, $max = 100),
        'created_at' => now()
    ];
});