<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BannersTableSeeder::class,
            NewsTableSeeder::class,
            CategoriesTableSeeder::class,
            StatusesTableSeeder::class,
            ProductsTableSeeder::class,
            UsersTableSeeder::class,
        ]);
    }
}
