<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title = 'Сабвуфер HERTZ DS 25.3';
        $desc = 'Высококачественный глубокий бас';
        DB::table('banners')->insert([
            'title' => $title,
            'description' => $desc,
            'link' => 'http://jnsound.test/product/1',
            'img_src' => md5($title.$desc).'.jpg'
        ]);
        $title = 'Сигнализация PANDORA LX 3257';
        $desc = 'Лучшая автосигнализация на данный момент';
        DB::table('banners')->insert([
            'title' => $title,
            'description' => $desc,
            'link' => 'http://jnsound.test/product/2',
            'img_src' => md5($title.$desc).'.png'
        ]);
    }
}
