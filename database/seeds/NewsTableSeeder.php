<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            [
                'title' => 'Black Friday Guide: Best Sales & Discount Codes',
                'description' => 'Duis ut velit gravida nibh bibendum commodo. Sus-pendisse pellentesque mattis augue id euismod. Inter-dum et malesuada fames',
                'img_src' => 'blog-01.jpg',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'The White Sneakers Nearly Every Fashion Girls Own',
                'description' => 'Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit ame',
                'img_src' => 'blog-01.jpg',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'New York SS 2018 Street Style: Annina Mislin',
                'description' => 'Proin nec vehicula lorem, a efficitur ex. Nam vehicula nulla vel erat tincidunt, sed hendrerit ligula porttitor. Fusce sit amet maximus nunc',
                'img_src' => 'blog-01.jpg',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
