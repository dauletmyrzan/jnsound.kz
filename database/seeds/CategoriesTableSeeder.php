<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Магнитолы',
                'code' => 'automagnitols',
                'img_src' => 'automagnitols.png'
            ],
            [
                'name' => 'Сигнализация',
                'code' => 'security',
                'img_src' => 'security.png'
            ],
            [
                'name' => 'Аксессуары',
                'code' => 'accessories',
                'img_src' => 'accessories.png'
            ],
            [
                'name' => 'Сабвуферы',
                'code' => 'subwoofers',
                'img_src' => 'subwoofers.png'
            ],
            [
                'name' => 'Усилители',
                'code' => 'usiliteli',
                'img_src' => 'usiliteli.png'
            ]
        ]);
    }
}
