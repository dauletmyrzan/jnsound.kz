<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Даулет',
            'email' => 'dauletmyrzan@gmail.com',
            'password' => '$2y$10$/Akh2aEsNdgF8hvLXAnF6eshLugswdkgsXe3KXDoN80SmDCkVAaKW',
            'role' => 'admin',
            'confirmed' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
